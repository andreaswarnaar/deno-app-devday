# deno-app
Sources :
 * https://medium.com/@kitsonk/develop-with-deno-and-visual-studio-code-225ce7c5b1ba 
 * https://deno.land/manual.html#denotypes
 * https://github.com/denoland/deno_std/blob/master/http/README.md
 * https://ejs.co/
 * https://github.com/syumai/dinatra
 * https://github.com/syumai/dejs
 * https://github.com/kitsonk/deno_example
 * https://www.typescriptlang.org/docs/handbook/module-resolution.html
  
#Requirements:
Linux OS
Docker & docker-compose

Run it :

`bash console.sh build`

`bash consele.sh start`

In the container terminal run ``` deno run --allow-all src/server.ts```

On your host you can test the server with curl:

```curl http://localhost:5589/```

# Dev setup VSCODE

To have type support for deno you need to follow these steps:

1 `yarn add typescript deno_ls_plugin` OR `yarn install`
2 Go to the CMD, Search for TypeScript and select local typescript in the node_modules folder to allow VSCODE use the correct version


NOTE: To resolve the correct path to the Deno modules the paths pointing to the docker deno-cache folder.
```
{
 "paths": {
   "http://*": ["docker/.deno-cache/deps/http/*"],
   "https://*": ["docker/.deno-cache/deps/https/*"]
}
```
When importing a new module for example: 
`https://deno.land/std/http/server.ts`
This path is redirected to `https://raw.githubusercontent.com/denoland/deno_std/master/http/server.ts 
The redirect information can be found in the cache folder: 
docker/.deno-cache/deps/https/deno.land/std/http/server.ts.headers.json 
