#!/usr/bin/env bash
# Ensure that the needed env variables are available
# Create dist folder and build the application

export CI_COMMIT_REF_NAME=${CI_COMMIT_REF_NAME:-""}
export CI_COMMIT_TAG=${CI_COMMIT_TAG:-""}

if [ "$CI_COMMIT_REF_NAME" == "master" ] && [ "$CI_COMMIT_TAG" == "" ]; then
  ng build -c acceptance
elif [ "$CI_COMMIT_TAG" != "" ]; then
    ng build -c production
else
    ng build -c testing
fi
