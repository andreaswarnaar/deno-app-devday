// https://deno.land/std/http/server.ts
import {serve, Server, ServerRequest,} from "https://raw.githubusercontent.com/denoland/deno_std/master/http/server.ts";
// https://deno.land/x/dejs@0.2.1/dejs.ts
import {renderFile} from 'https://raw.githubusercontent.com/syumai/dejs/0.2.1/dejs.ts';

const {open, cwd} = Deno;
const server: Server = serve("0.0.0.0:8080");
const encoder = new TextEncoder();

function logger(message, log): Promise<any> {
    return log.write(encoder.encode(message+"\n"));
}

async function main() {
    const log = await open("request.log", "a+");

    for await (const req of server) {
        const serverRequest: ServerRequest = req;

        await logger(serverRequest.url, log);
        let responseContent = await renderFile(`${cwd()}/src/templates/layout.ejs`, {
            name: 'world',
        });
        serverRequest.respond({body: responseContent});
    }
}

main();
