#!/usr/bin/env bash -e

# Set environment variables for dev
export PROJECT_NAME='deno'
export NODE_ENV=${NODE_ENV:-dev}
export DENO_VERSION='v0.2.10'
export APP_PORT=${APP_PORT:-5589}
export COLUMNS=${COLUMNS:-300}

if [ -f .env ] ;then
    export $(egrep -v '^#' .env | xargs);
fi

echo -e '\033]2;'$PROJECT_NAME Console'\007'

COMPOSE="docker-compose -f docker/docker-compose.yml"

if [ ! -f 'docker/.bash_history' ]; then
    echo 'bash history create before mounting it into the docker container'
    touch docker/.bash_history
fi

if [ ! -d 'docker/.deno-cache' ]; then
    echo 'deno cache folder created and mounting it into the docker container'
    mkdir docker/.deno-cache
fi

if [ ! -f '.env' ]; then
    echo 'Copy .env.dist to .env'
    cp ./.env.dist .env
fi


# If we pass any arguments...
if [ $# -gt 0 ];then

    if [ "$1" == "start" ]; then
        echo "RUN deno types > lib/lib.deno_runtime.d.ts when updating"
        $COMPOSE up -d
        $COMPOSE exec node bash

    elif [ "$1" == "node" ]; then

        $COMPOSE exec node bash

    elif [ "$1" == "restart" ]; then
        shift 1
        $COMPOSE down && $COMPOSE up "$@"

    elif [ "$1" == "start" ]; then
        shift 1
        $COMPOSE up -d && $COMPOSE exec node bash
    else
        $COMPOSE "$@"
    fi
else
    $COMPOSE ps
fi
