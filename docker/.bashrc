# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# Note: PS1 and umask are already set in /etc/profile. You should not
# need this unless you want different defaults for root.

PS1='\[\033[01;36m\]\w\[\033[00m\]\n${debian_chroot:+($debian_chroot)}\[\033[01;36m\]$NAME \D{%T} :: \[\033[00m\]'
# umask 022

export HOSTIP=`/sbin/ip route|awk '/default/ { print $3 }'`
export PATH="$HOME/.yarn/bin:$HOME/.config/yarn/global/node_modules/.bin:$PATH"
export PATH="/node/.deno/bin:$PATH"

# You may uncomment the following lines if you want `ls' to be colorized:
export LS_OPTIONS='--color=auto'
# eval "`dircolors`"
alias ls='ls $LS_OPTIONS'
alias ll='ls $LS_OPTIONS -l'
alias l='ls $LS_OPTIONS -lA'

# Some more alias to avoid making mistakes:
#alias rm='rm -i'
#alias cp='cp -i'
#alias mv='mv -i'

HISTCONTROL=ignoreboth
shopt -s histappend
PROMPT_COMMAND="history -a;$PROMPT_COMMAND"
HISTSIZE=4000
HISTFILESIZE=8000
HISTTIMEFORMAT="%d/%m/%y %T "

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

